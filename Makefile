build: keyrings/kali-archive-keyring.gpg

keyrings/kali-archive-keyring.gpg: active-keys/index
	mkdir -p keyrings
	jetring-build -I $@ active-keys

clean:
	rm -rf keyrings

install: build
	install -d $(DESTDIR)/usr/share/keyrings/
	cp keyrings/*.gpg $(DESTDIR)/usr/share/keyrings/

.PHONY: build clean install
